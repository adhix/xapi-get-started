<?php

$loader = require 'vendor/autoload.php';

$lrs = new TinCan\RemoteLRS(
    'https://cloud.scorm.com/tc/OCLI151LXR/',
    '1.0.0',
    'wUJbYOWG6BwyvMuEfMo',
    'o2RSPI2xqZZRWxuXH_c'
);


$actor = new TinCan\Agent(
    [ 'mbox' => 'mailto:info@tincanapi.com' ]
);
$verb = new TinCan\Verb(
    [ 'id' => 'http://adlnet.gov/expapi/verbs/experienced' ]
);
$activity = new TinCan\Activity(
    [ 'id' => 'http://rusticisoftware.github.com/TinCanPHP' ]
);
$statement = new TinCan\Statement(
    [
        'actor' => $actor,
        'verb'  => $verb,
        'object' => $activity,
    ]
);

$response = $lrs->saveStatement($statement);
if ($response->success) {
    print "Statement sent successfully!\n";
}
else {
    print "Error statement not sent: " . $response->content . "\n";
}

echo "<pre>";
print_r($response);
echo "</pre>";
